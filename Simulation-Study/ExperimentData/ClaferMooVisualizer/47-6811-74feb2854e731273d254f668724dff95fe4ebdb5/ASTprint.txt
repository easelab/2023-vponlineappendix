RepositoryCopies Version :7242
  ClaferMooVisualizer Version :1019
    FeatureMapping.json Version :3080
      Block Version :3080
    Talk_Samples Version :6279
      test1.cfr Version :6280
        test1.cfr-block1 Version :6280
      Non-AFM.cfr Version :1293
        Non-AFM.cfr-block1 Version :1293
      ClaferMooVizPaperExample_Existing1.data Version :540
        ClaferMooVizPaperExample_Existing1.data-block1 Version :540
      ClaferMooVizPaperExample_Existing1.cfr Version :538
        ClaferMooVizPaperExample_Existing1.cfr-block1 Version :538
      ClaferMooVizPaperExample_Downgraded_Front.data Version :536
        ClaferMooVizPaperExample_Downgraded_Front.data-block1 Version :536
      ClaferMooVizPaperExample_Downgraded_Front.cfr Version :534
        ClaferMooVizPaperExample_Downgraded_Front.cfr-block1 Version :534
      ClaferMooVizPaperExample.data Version :532
        ClaferMooVizPaperExample.data-block1 Version :532
      ClaferMooVizPaperExample.cfr Version :530
        ClaferMooVizPaperExample.cfr-block1 Version :530
      AndroidSampleMoo_5.cfr Version :528
        AndroidSampleMoo_5.cfr-block1 Version :528
      AndroidSampleMoo_4_existing_product_spec.cfr Version :526
        AndroidSampleMoo_4_existing_product_spec.cfr-block1 Version :526
      AndroidSampleMoo_4.cfr Version :524
        AndroidSampleMoo_4.cfr-block1 Version :524
      AndroidSampleMoo_3.cfr Version :522
        AndroidSampleMoo_3.cfr-block1 Version :522
      AndroidSampleMoo_2.cfr.2.data Version :520
        AndroidSampleMoo_2.cfr.2.data-block1 Version :520
      AndroidSampleMoo_2.cfr.1.data Version :518
        AndroidSampleMoo_2.cfr.1.data-block1 Version :518
      AndroidSampleMoo_2.cfr Version :516
        AndroidSampleMoo_2.cfr-block1 Version :516
    Server Version :5444
      cache Version :6737
      Backends Version :6281
        backends.json Version :6282
          Block Version :6282
        ChocoSingle Version :6741
        ClaferMoo Version :6739
          spl_datagenerator Version :5455
          tools Version :5441
      Examples Version :4910
        examples.json Version :4911
          Block Version :4911
        MOO_MobilePhone_From_Wiki.cfr Version :4491
          MOO_MobilePhone_From_Wiki.cfr-block1 Version :4491
        MOO_Android4_From_Wiki.cfr Version :4489
          MOO_Android4_From_Wiki.cfr-block1 Version :4489
        AndroidSampleMoo_2.cfr Version :4487
          AndroidSampleMoo_2.cfr-block1 Version :4487
        AndroidSampleMoo_3.cfr Version :4485
          AndroidSampleMoo_3.cfr-block1 Version :4485
        AndroidSampleMoo_4.cfr Version :4483
          AndroidSampleMoo_4.cfr-block1 Version :4483
        AndroidSampleMoo_5.cfr Version :4481
          AndroidSampleMoo_5.cfr-block1 Version :4481
        Car4D.cfr Version :4479
          Car4D.cfr-block1 Version :4479
      uploads Version :513
      server.js Version :7105
        block Version :7104
        block Version :7103
        block Version :7101
        block Version :7100
        block Version :7096
        block Version :7086
        block Version :7085
        block Version :7084
        block Version :7081
        block Version :7078
        block Version :7075
        block Version :7072
        block Version :7071
        block Version :7068
        block Version :7067
        block Version :7064
        block Version :7063
        block Version :7025
        block Version :6987
        block Version :6984
        block Version :6981
        block Version :6980
        block Version :6977
        block Version :6974
        block Version :6973
        block Version :6970
        block Version :6967
        block Version :6964
        block Version :6961
        block Version :6960
        block Version :6957
        block Version :6956
        block Version :7046
        block Version :6952
        block Version :6949
        block Version :6948
        block Version :6945
        block Version :6944
        block Version :6941
        block Version :6940
        block Version :7088
        block Version :6936
        block Version :6934
        block Version :6933
        block Version :6932
        block Version :6896
        block Version :6893
        block Version :6895
        block Version :6891
        block Version :6887
        block Version :6883
        block Version :6882
        block Version :6878
        block Version :6874
        block Version :6873
        block Version :6869
        block Version :6833
        block Version :6829
        finishCleanup(dir, Version :6828
          Block Version :7092
        cleanupOldFiles(path, Version :6827
          Block Version :7093
        deleteOld(path){ Version :6826
          Block Version :7094
        escapeJSON(unsafe) Version :6825
          Block Version :7058
        changeFileExt(name, Version :6824
          Block Version :7059
        killProcessTree(process) Version :6823
          Block Version :7099
        dependency_ok() Version :6822
          Block Version :7106
        filterArgs(original_args, Version :6821
          Block Version :7062
        Block Version :7089
        Block Version :7090
        Block Version :7091
        Block Version :7051
        Block Version :7095
      package.json Version :500
        package.json-block1 Version :500
      config.json Version :6587
        block Version :6586
        block Version :6588
        block Version :6584
        block Version :6580
      Client Version :4916
        compiler_html_wrapper.html Version :4917
          compiler_html_wrapper.html-block1 Version :4917
        md_clafermodel.js Version :7238
          ClaferModel(host) Version :7238
            Block Version :0
          Block Version :7237
          Block Version :7236
        jquery.html5-placeholder-shim.js Version :2600
          jquery.html5-placeholder-shim.js-block1 Version :2600
        instance_parser.js Version :1023
          InstanceParser(instances) Version :1023
            Block Version :0
          Block Version :1022
        styles.css Version :6590
          styles.css-block1 Version :6590
        selector.js Version :2732
          Selector(host) Version :2732
            Block Version :0
          Block Version :2731
          Block Version :2730
        robots.txt Version :490
          robots.txt-block1 Version :490
        md_usecases.js Version :488
          Block Version :488
          Block Version :487
          UseCases(host) Version :486
            Block Version :0
        md_input.js Version :7228
          block Version :7227
          block Version :7226
          block Version :7225
          block Version :7222
          block Version :7221
          block Version :7218
          block Version :7217
          block Version :7215
          block Version :7214
          block Version :7211
          block Version :7210
          block Version :7207
          block Version :7206
          block Version :7203
          block Version :7202
          block Version :7199
          block Version :7198
          block Version :7194
          block Version :7191
          block Version :7190
          block Version :7188
          block Version :7187
          block Version :7184
          block Version :7183
          block Version :7178
          block Version :7177
          block Version :7176
          block Version :7173
          block Version :7172
          block Version :7169
          block Version :7168
          block Version :7165
          block Version :7164
          block Version :7161
          block Version :7158
          block Version :7157
          block Version :7153
          block Version :7150
          block Version :7149
          block Version :7146
          block Version :7145
          block Version :7142
          block Version :7139
          block Version :7138
          block Version :7135
          block Version :7132
          block Version :7131
          block Version :7128
          block Version :7125
          block Version :7124
          block Version :7121
          block Version :7120
          Input(host) Version :7119
            block Version :7118
            block Version :7117
            block Version :7116
            block Version :7112
          unescapeJSON(escaped) Version :7110
            Block Version :0
          Block Version :7109
        md_graph.js Version :7234
          Graph(host) Version :7234
            Block Version :0
          setDim(el, Version :7233
            Block Version :0
          Block Version :7232
          Block Version :7231
          Block Version :7230
        md_goals.js Version :5855
          Goals(host) Version :5855
            Block Version :0
          Block Version :5854
          Block Version :5853
        md_console.js Version :2084
          Console(host) Version :2084
            Block Version :0
          Block Version :2083
          Block Version :2082
        md_comparison_table.js Version :2826
          ComparisonTable(host) Version :2826
            Block Version :0
          Block Version :2825
          Block Version :2824
        md_analysis.js Version :5859
          Analysis(host) Version :5859
            Block Version :0
          Block Version :5858
          Block Version :5857
        main.js Version :6726
          block Version :6725
          block Version :6724
          block Version :6723
          Host(modules) Version :6722
            Block Version :0
          Block Version :6721
        instance_filter.js Version :2842
          InstanceFilter Version :2842
            Block Version :0
          Block Version :2841
          Block Version :2840
        images Version :449
          verticalAxis.png Version :450
            verticalAxis.png-block1 Version :450
          tick.png Version :448
            tick.png-block1 Version :448
          removeMouseOver.png Version :446
            removeMouseOver.png-block1 Version :446
          remove.png Version :444
            remove.png-block1 Version :444
          preloader.gif Version :442
            preloader.gif-block1 Version :442
          no.png Version :440
            no.png-block1 Version :440
          horizontalAxis.png Version :438
            horizontalAxis.png-block1 Version :438
          help_hover.png Version :436
            help_hover.png-block1 Version :436
          help.png Version :434
            help.png-block1 Version :434
          checkbox_x.bmp Version :432
            checkbox_x.bmp-block1 Version :432
          checkbox_ticked_greyed.png Version :430
            checkbox_ticked_greyed.png-block1 Version :430
          checkbox_ticked.bmp Version :428
            checkbox_ticked.bmp-block1 Version :428
          checkbox_empty.bmp Version :426
            checkbox_empty.bmp-block1 Version :426
        help_pages Version :4918
          clafersourcemodel_help.html Version :4919
            clafersourcemodel_help.html-block1 Version :4919
          objectivesandqualityranges_help.html Version :3084
            objectivesandqualityranges_help.html-block1 Version :3084
          inputfileorexample_help.html Version :3082
            inputfileorexample_help.html-block1 Version :3082
          variantcomparer_help.html Version :3068
            variantcomparer_help.html-block1 Version :3068
          start_help.html Version :7240
            start_help.html-block1 Version :7240
          objectives_help.html Version :3072
            objectives_help.html-block1 Version :3072
          input_help.html Version :3074
            input_help.html-block1 Version :3074
          featureandqualitymatrix_help.html Version :3076
            featureandqualitymatrix_help.html-block1 Version :3076
          bubblefrontgraph_help.html Version :3320
            bubblefrontgraph_help.html-block1 Version :3320
        commons Version :407
          xml_helper.js Version :410
            Block Version :410
            Block Version :409
            XMLHelper() Version :408
              Block Version :0
          set Version :349
            src Version :405
              tsort.js Version :406
                Block Version :406
              test.js Version :404
                Block Version :404
              state.js Version :402
                Block Version :402
              stack_trace.js Version :400
                Block Version :400
              set.js Version :398
                Block Version :398
              range.js Version :396
                Block Version :396
              proxy.js Version :394
                Block Version :394
              package.js Version :392
                Block Version :392
              package-browser.js Version :390
                Block Version :390
              observable.js Version :388
                Block Version :388
              method_chain.js Version :386
                Block Version :386
              loader.js Version :384
                Block Version :384
              loader-browser.js Version :382
                Block Version :382
              linked_list.js Version :380
                Block Version :380
              hash.js Version :378
                Block Version :378
              forwardable.js Version :376
                Block Version :376
              enumerable.js Version :374
                Block Version :374
              dom.js Version :372
                Block Version :372
              deferrable.js Version :370
                Block Version :370
              decorator.js Version :368
                Block Version :368
              core.js Version :366
                Block Version :366
              constant_scope.js Version :364
                Block Version :364
              console.js Version :362
                Block Version :362
              comparable.js Version :360
                Block Version :360
              command.js Version :358
                Block Version :358
              benchmark.js Version :356
                Block Version :356
              assets Version :353
                testui.css Version :354
                  testui.css-block1 Version :354
                bullet_go.png Version :352
                  bullet_go.png-block1 Version :352
            README.txt Version :348
              README.txt-block1 Version :348
            package.json Version :346
              package.json-block1 Version :346
            min Version :343
              tsort.js.map Version :344
                tsort.js.map-block1 Version :344
              tsort.js Version :342
                Block Version :342
              test.js.map Version :340
                test.js.map-block1 Version :340
              test.js Version :338
                Block Version :338
              state.js.map Version :336
                state.js.map-block1 Version :336
              state.js Version :334
                Block Version :334
              stack_trace.js.map Version :332
                stack_trace.js.map-block1 Version :332
              stack_trace.js Version :330
                Block Version :330
              set.js.map Version :328
                set.js.map-block1 Version :328
              set.js Version :326
                Block Version :326
              range.js.map Version :324
                range.js.map-block1 Version :324
              range.js Version :322
                Block Version :322
              proxy.js.map Version :320
                proxy.js.map-block1 Version :320
              proxy.js Version :318
                Block Version :318
              package.js.map Version :316
                package.js.map-block1 Version :316
              package.js Version :314
                Block Version :314
              package-browser.js.map Version :312
                package-browser.js.map-block1 Version :312
              package-browser.js Version :310
                Block Version :310
              observable.js.map Version :308
                observable.js.map-block1 Version :308
              observable.js Version :306
                Block Version :306
              method_chain.js.map Version :304
                method_chain.js.map-block1 Version :304
              method_chain.js Version :302
                Block Version :302
              loader.js.map Version :300
                loader.js.map-block1 Version :300
              loader.js Version :298
                Block Version :298
              loader-browser.js.map Version :296
                loader-browser.js.map-block1 Version :296
              loader-browser.js Version :294
                Block Version :294
              linked_list.js.map Version :292
                linked_list.js.map-block1 Version :292
              linked_list.js Version :290
                Block Version :290
              hash.js.map Version :288
                hash.js.map-block1 Version :288
              hash.js Version :286
                Block Version :286
              forwardable.js.map Version :284
                forwardable.js.map-block1 Version :284
              forwardable.js Version :282
                Block Version :282
              enumerable.js.map Version :280
                enumerable.js.map-block1 Version :280
              enumerable.js Version :278
                Block Version :278
              dom.js.map Version :276
                dom.js.map-block1 Version :276
              dom.js Version :274
                Block Version :274
              deferrable.js.map Version :272
                deferrable.js.map-block1 Version :272
              deferrable.js Version :270
                Block Version :270
              decorator.js.map Version :268
                decorator.js.map-block1 Version :268
              decorator.js Version :266
                Block Version :266
              core.js.map Version :264
                core.js.map-block1 Version :264
              core.js Version :262
                Block Version :262
              constant_scope.js.map Version :260
                constant_scope.js.map-block1 Version :260
              constant_scope.js Version :258
                Block Version :258
              console.js.map Version :256
                console.js.map-block1 Version :256
              console.js Version :254
                Block Version :254
              comparable.js.map Version :252
                comparable.js.map-block1 Version :252
              comparable.js Version :250
                Block Version :250
              command.js.map Version :248
                command.js.map-block1 Version :248
              command.js Version :246
                Block Version :246
              benchmark.js.map Version :244
                benchmark.js.map-block1 Version :244
              benchmark.js Version :242
                Block Version :242
              assets Version :239
                testui.css Version :240
                  testui.css-block1 Version :240
                bullet_go.png Version :238
                  bullet_go.png-block1 Version :238
            LICENSE.txt Version :234
              LICENSE.txt-block1 Version :234
            index.js Version :232
              Block Version :232
            CHANGELOG.txt Version :230
              CHANGELOG.txt-block1 Version :230
            bin Version :227
              jsbuild Version :228
                jsbuild-block1 Version :228
          ptable.js Version :224
            Block Version :224
            Block Version :223
            TableVisualizer Version :222
              Block Version :0
          plist.js Version :220
            Block Version :220
            Block Version :219
            ListVisualizer Version :218
              Block Version :0
          pfront.js Version :6730
            ParetoFrontVisualizer Version :6730
              Block Version :0
            Block Version :6729
            Block Version :6728
          jquery.form.js Version :212
            jquery.form.js-block1 Version :212
          jquery-windows-engine Version :209
            sample.html Version :210
              sample.html-block1 Version :210
            jquery.windows-engine.js Version :208
              jquery.windows-engine.js-block1 Version :208
            jquery.windows-engine.css Version :206
              jquery.windows-engine.css-block1 Version :206
            default Version :203
              top_right.gif Version :204
                top_right.gif-block1 Version :204
              top_mid.gif Version :202
                top_mid.gif-block1 Version :202
              top_left.gif Version :200
                top_left.gif-block1 Version :200
              sizer.gif Version :198
                sizer.gif-block1 Version :198
              minimize.gif Version :196
                minimize.gif-block1 Version :196
              maximize.gif Version :194
                maximize.gif-block1 Version :194
              close.gif Version :192
                close.gif-block1 Version :192
              bottom_mid.gif Version :190
                bottom_mid.gif-block1 Version :190
            ajaxcontent.html Version :187
              ajaxcontent.html-block1 Version :187
          jquery-1.8.2.js Version :184
            jquery-1.8.2.js-block1 Version :184
          instance_processor.js Version :182
            Block Version :182
            Block Version :181
            InstanceProcessor Version :180
              Block Version :0
          instance_converter.js Version :178
            Block Version :178
            Block Version :177
            InstanceConverter(source) Version :176
              Block Version :0
          init.js Version :174
            Block Version :174
            Block Version :173
            Block Version :172
            Block Version :171
            Block Version :170
            isNumeric(input) Version :169
              Block Version :0
            getPID(input){ Version :168
              Block Version :0
            parsePID(PID){ Version :167
              Block Version :0
            setCookie(c_name,value,exdays) Version :166
              Block Version :0
            getCookie(c_name) Version :165
              Block Version :0
          help_getter.js Version :163
            Block Version :163
            helpGetter(host){ Version :162
              Block Version :0
          data.js Version :160
            Block Version :160
            Block Version :159
            DataTable Version :158
              Block Version :0
          clafer_processor.js Version :2846
            ClaferProcessor Version :2846
              Block Version :0
            Block Version :2845
            Block Version :2844
        app.html Version :5321
          app.html-block1 Version :5321
      ClaferMoo Version :148
    README.md Version :7242
      README.md-block1 Version :7242
    LICENSE Version :144
      LICENSE-block1 Version :144
    Files Version :6811
