AddFeatureToFeatureModel - Feature added : server-fileUpload-compileErrorHandling
Move Feature - Feature automaticViewSizing moved from Some(UnAssigned) to client
AddFeatureToFeatureModel - Feature added : client-views-evolutionController
Move Feature - Feature client-views-bubbleFrontGraph-handleOptimalAndExistingInst moved from Some(client-views-bubbleFrontGraph) to client-views-evolutionController
RemoveFeature - Feature inactivityTimeoutTimeout removed from feature model
RemoveFeature - Feature filteringByGoals removed from feature model
RemoveFeature - Feature inactivityTimeoutTimeout removed from feature model
RemoveFeature - Feature filteringByGoals removed from feature model
MapAssetToFeature - File C:\Experiment\RepositoryCopies\ClaferMooVisualizer\Server\Client\selection_controller.js mapped to feature client-views-multipleSelection
MapAssetToFeature - File C:\Experiment\RepositoryCopies\ClaferMooVisualizer\Server\Client\instance_filter.js mapped to feature client-views-featureAndQualityMatrix-contentFilter
MapAssetToFeature - File C:\Experiment\RepositoryCopies\ClaferMooVisualizer\Server\Client\evolution_controller.js mapped to feature client-views-evolutionController
