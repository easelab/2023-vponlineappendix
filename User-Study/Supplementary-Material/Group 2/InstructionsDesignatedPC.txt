Experiment instructions:

The goal of this experiment is to compare the correctness and efficiency of performing routine software evolution tasks manually, and using our framework called the virtual platform. The experiment consists of 3 parts:

1.	Familiarizing yourself with the subject systems used in the experiment.
2.	Solving the tasks using virtual platform, and then without using virtual platform, filling the given questionnaire along the way. Questionnaire found at: https://forms.gle/Nc637A8vYJapqATi8. A hard-copy of the form is also given to you, but the purpose of that is to minimize screen switching. You have to fill the online questionnaire in the end. You can also directly will the online questionnaire right away since you have multiple screens (we'll recommend that so you save time).
3.	Participating in the follow-up interview.

Instructions:

1.	Read the handouts with "Subject Systems' Introduction" written on top of the first page to familiarize yourself with the subject systems. You can skip this step if you have already done so.
2.	View the file "VPIntroduction.pptx" on the Desktop in slideshow mode. You can skip this step if you have already done so.
3.  Open the questionnaire, read the instructions, and give responses to the questions in Section 1 (participation and consent) and Section 2 (demographics).
4.	Read the handouts titled "Operator descriptions". You can skip this step if you have already done so.
5.  Click "start recording" on OBS. Use the screen OBS is recording for your tasks. 
6. Open the command line interface by searching for �cmd� in the search bar for windows.
7. Check if virtual platform works by typing vp help. It should display a list of commands with their descriptions (ignore the sbt messages and warnings, if any).
8.	Perform tasks in section 3 and 4 using virtual platform (via command-line interface) and provide your responses in the questionnaire. VP command-line interface path is already set to the root of the DART project, you can directly start using the command-line interface. Please be mindful of recording times.
9.	Stop recording and take a 15 minute break.
10.	Start recording again, and perform tasks in section 5 and 6 manually (using IntelliJ that is already opened) and provide responses in the questionnaire. Please be mindful of recording the times. 
11.	Stop recording and fill the remainder of the questionnaire (section 7 and 8) by providing your subjective preferences and reasoning.
12.	Let the person conducting the experiment know that you are done, so you can take part in a one-to-one follow-up interview with them.

Note: Feel free to use a separate word file to copy paste some task responses before you fill them in the questionnaire.
