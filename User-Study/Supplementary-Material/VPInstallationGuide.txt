VP Installation guide

1. Clone VP from easelab: git clone https://bitbucket.org/easelab/vp.git
2. Add path of the cloned vp to your system variables Path e.g., C:\vpexp\vp (search for environment variables, go to system variables, go to path, and add the path to vp there).
3. Test that you can run vp. From the command line, type vp help. Ignore the warnings.
4. If you get the message "sbt not recognized", then install sbt (link: https://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Windows.html).
5. After installing sbt, close the previous comand window if still open. Then open a new command window and rerun the command "vp help"
6. If it still does not work, please email me at wardah@chalmers.se