package Persons;

import IO.Tools;
import Items.Game;
import Items.Rentable;
import Items.Song;
import Utilities.*;

import java.io.*;
import java.time.Year;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Manager {
    public void readFile(ArrayList<Employee> employees, ArrayList<Customer> customers, ArrayList<Rentable> items, Scanner input) {
        try {
            File dartData = new File("dartData.txt");
            FileReader fr = new FileReader(dartData);
            BufferedReader br = new BufferedReader(fr);

            String line;
            while((line = br.readLine()) != null) {
                String[] dartInfo = line.split(";");
                String var10 = dartInfo[0];
                byte var11 = -1;
                switch(var10.hashCode()) {
                    case -1487370466:
                        if (var10.equals("Employee")) {
                            var11 = 0;
                        }
                        break;
                    case 2211858:
                        if (var10.equals("Game")) {
                            var11 = 1;
                        }
                        break;
                    case 2582837:
                        if (var10.equals("Song")) {
                            var11 = 2;
                        }
                        break;
                    case 670819326:
                        if (var10.equals("Customer")) {
                            var11 = 3;
                        }
                }

                switch(var11) {
                    case 0:
                        this.registerEmployee(employees, dartInfo[1], dartInfo[2], Integer.parseInt(dartInfo[3]), Double.parseDouble(dartInfo[4]), input);
                        break;
                    case 1:
                        items.add(new Game(dartInfo[1], dartInfo[2], Double.parseDouble(dartInfo[3]), Integer.parseInt(dartInfo[4])));
                        break;
                    case 2:
                        items.add(new Song(dartInfo[1], dartInfo[2], Double.parseDouble(dartInfo[3]), Integer.parseInt(dartInfo[4])));
                        break;
                    case 3:
                        customers.add(new Customer(dartInfo[1], dartInfo[2]));
                }
            }
        } catch (Exception var12) {
            System.out.println("File does not exist");
        }

    }
    public void writeFile(ArrayList<RentHistoryItem> rentHistoryItems) throws IOException {
        BufferedWriter bfWriter = new BufferedWriter(new FileWriter(new File("rentTransactions.txt")));
        Iterator var3 = rentHistoryItems.iterator();

        while(var3.hasNext()) {
            RentHistoryItem rentHistoryItem = (RentHistoryItem)var3.next();
            bfWriter.write(rentHistoryItem.toFileString() + "\n");
        }

        bfWriter.close();
    }
    public void registerEmployee(ArrayList<Employee> employees, String name, String address, int bYear, double salary, Scanner input) {
        Employee employee = null;
        boolean inCorrect;

        do{
            try {
                employee = new Employee(name, address, bYear, salary);
                inCorrect = false;
            } catch (NameEmptyException e) {
                inCorrect = true;
                name = Tools.getString("Please input name again: ", input);
            } catch (NegativeSalaryException e){
                inCorrect = true;
                salary = Tools.getDouble("Please input salary again: ");
            }
        } while (inCorrect || employee == null);

        employees.add(employee);
    }
    public boolean removeEmployee(ArrayList<Employee> employeeArrayList, Employee removeEmployee) {
        return employeeArrayList.remove(removeEmployee);
    }
    public Employee findEmployee(ArrayList<Employee> employeeArrayList, String ID) {
        for (Employee employee : employeeArrayList)
            if (employee.getID().equals(ID))
                return employee;
        System.out.println("Persons.Employee with id " + ID + " not found.");
        return null;
    }
    public double calcNetSalary(Employee employee) {
        if (employee == null) return 0;
        final double GROSS_SALARY_TAX = 0.7;
        final int TAX_CONDITION = 100000;

        if (employee.getGrossSalary() >= TAX_CONDITION)
            employee.setNetSalary(employee.getGrossSalary() * GROSS_SALARY_TAX);
        else employee.setNetSalary(employee.getGrossSalary());

        return employee.getNetSalary();
    }
    public int bonus(Employee employee) {
        final int[] BONUS = new int[]{4000, 6000, 7500};
        final int[] YEAR_CONDITION = new int[]{22, 30};
        int bonus;

        if ((Year.now().getValue()-employee.getBirthyear()) < YEAR_CONDITION[0]) { bonus = BONUS[0]; }
        else if ((Year.now().getValue()-employee.getBirthyear()) < YEAR_CONDITION[1]) { bonus = BONUS[1]; }
        else bonus = BONUS[2];
        employee.setNetSalary(employee.getNetSalary() + bonus);

        return bonus;
    }
    public String viewAllEmployee(ArrayList<Employee> employeeArrayList) {
        String empStr = "";
        for (Employee manager : employeeArrayList) {
            empStr = empStr.concat(manager.toString() + System.lineSeparator());
        }
        return empStr;
    }
    public String viewRentFrequency(ArrayList<Rentable> itemsList) {
        String rentFreq = "";

        for (Rentable game : itemsList) {
            if(game.getRentFrequency() > 0 ){
                rentFreq = rentFreq.concat(game.getTitle() + " : " + game.getRentFrequency()) + System.lineSeparator();
            }
        }
        for (Rentable song : itemsList) {
            if(song.getRentFrequency() > 0 ){
                rentFreq = rentFreq.concat(song.getTitle() + " : " + song.getRentFrequency());
            }
        }
        return rentFreq;
    }
    public Rentable mostProfitable(ArrayList<Rentable> itemsList) {
        Rentable mostProfit = null;

        if(itemsList.size() > 0) {
            mostProfit = itemsList.get(0);
            for (Rentable rentable : itemsList) {
                if (rentable.getProfit() > mostProfit.getProfit()) {
                    mostProfit = rentable;
                }
            }
        }
        return mostProfit;
    }
    public Customer mostProfitableCustomer(ArrayList<Customer> customerList) {
        if (customerList.size() > 0) {
            Customer mostProfit = customerList.get(0);
            for (Customer customer : customerList)
                if (customer.getAmountSpent() > customer.getAmountSpent())
                    mostProfit = customer;
            return mostProfit;
        }
        return null;
    }
}
