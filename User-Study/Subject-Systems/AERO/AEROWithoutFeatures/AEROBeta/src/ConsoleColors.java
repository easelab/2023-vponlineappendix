import java.util.Scanner;

public class ConsoleColors {
    // Reset
    public String RESET = "\033[0m"; // Text Reset

    // Bold
    public String font1 = "\033[1;30m"; // BLACK
    public String font2 = "\033[1;31m"; // RED

    // Background
    public String colorBackground1 = "\033[48;5;243m"; // GRAY
    public String colorBackground2 = "\033[0;101m";// RED
    public String colorBackground3 = "\033[43m"; // YELLOW

    private void print_header() {
        System.out.println(this.colorBackground1 + "<><><><><><><><><><><><><><><><><><><><>" + this.RESET);
        System.out.println(this.colorBackground2 + this.font1 + "       Airline Reservation System       " + this.RESET);
        System.out.println(this.colorBackground1 + "<><><><><><><><><><><><><><><><><><><><>" + this.RESET);
        System.out.print("\n");
    }
    private void main_menu() {
        System.out.println(this.colorBackground3 + this.font1 + "---------->  Main Menu  <-----------" + this.RESET);
        System.out.println(this.colorBackground1 + this.font1 + "1- Members Management Menu          " + this.RESET);
        System.out.println(this.colorBackground1 + this.font1 + "2- Flight Management Menu           " + this.RESET);
        System.out.println(this.colorBackground1 + this.font2 + "3- Exit System                      " + this.RESET);
        System.out.println(this.colorBackground3 + this.font1 + "------------------------------------" + this.RESET);
    }
    public void customize(){
        System.out.println("Welcome to the color customization menu. Right now, your menus look like this:");
        print_header();
        main_menu();

        System.out.println("You can customize any of the three backgrounds");
            System.out.println(this.colorBackground1 + this.font1 + "1. Background 1" + this.RESET);
            System.out.println(this.colorBackground2 + this.font1 + "2. Background 2" + this.RESET);
            System.out.println(this.colorBackground3 + this.font1 + "3. Background 3" + this.RESET);

            int choice = 0;
            Scanner input = new Scanner(System.in);
            choice = input.nextShort();
            final String BLACK_BACKGROUND = "\033[40m";  // BLACK
            final String RED_BACKGROUND = "\033[41m";    // RED
            final String GREEN_BACKGROUND = "\033[42m";  // GREEN
            final String YELLOW_BACKGROUND = "\033[43m"; // YELLOW
            final String BLUE_BACKGROUND = "\033[44m";   // BLUE
            final String PURPLE_BACKGROUND = "\033[45m"; // PURPLE
            final String CYAN_BACKGROUND = "\033[46m";   // CYAN
            final String WHITE_BACKGROUND = "\033[47m";  // Gray
            if(choice > 0 && choice < 4){
                System.out.println("Here are your options: ");
                System.out.println(BLACK_BACKGROUND + this.font2  + "1. Black  " + this.RESET);
                System.out.println(RED_BACKGROUND + this.font1    + "2. Red    " + this.RESET);
                System.out.println(GREEN_BACKGROUND + this.font1  + "3. Green  " + this.RESET);
                System.out.println(YELLOW_BACKGROUND + this.font1 + "4. Yellow " + this.RESET);
                System.out.println(BLUE_BACKGROUND + this.font1   + "5. Blue   " + this.RESET);
                System.out.println(PURPLE_BACKGROUND + this.font1 + "6. Purple " + this.RESET);
                System.out.println(CYAN_BACKGROUND + this.font1   + "7. Cyan   " + this.RESET);
                System.out.println(WHITE_BACKGROUND + this.font1  + "8. Gray   " + this.RESET);

                System.out.println("Enter the color you like (1-8): ");
                int option = 0;
                option = input.nextShort();

                if(option > 0 && option < 9){

                    switch (option){
                        case 1:
                            if(choice == 1 ) colorBackground1 = "\033[40m";
                            else if(choice == 2) colorBackground2 = "\033[40m";
                            else colorBackground3 =  "\033[40m";
                            System.out.println("Background color changed to black. The menus look like this:");
                            System.out.println();
                            print_header();
                            main_menu();
                            break;
                        case 2:
                            if(choice == 1 ) colorBackground1 = "\033[41m";
                            else if(choice == 2) colorBackground2 = "\033[41m";
                            else colorBackground3 =  "\033[41m";
                            System.out.println("Background color changed to red. The menus look like this:");
                            System.out.println();
                            print_header();
                            main_menu();
                            break;
                        case 3:
                            if(choice == 1 ) colorBackground1 = "\033[42m";
                            else if(choice == 2) colorBackground2 = "\033[42m";
                            else colorBackground3 =  "\033[42m";
                            System.out.println("Background color changed to green. The menus look like this:");
                            System.out.println();
                            print_header();
                            main_menu();
                            break;
                        case 4:
                            if(choice == 1 ) colorBackground1 = "\033[43m";
                            else if(choice == 2) colorBackground2 = "\033[43m";
                            else colorBackground3 =  "\033[43m";
                            System.out.println("Background color changed to yellow. The menus look like this:");
                            System.out.println();
                            print_header();
                            main_menu();
                            break;
                        case 5:
                            if(choice == 1 ) colorBackground1 = "\033[44m";
                            else if(choice == 2) colorBackground2 = "\033[44m";
                            else colorBackground3 =  "\033[44m";
                            System.out.println("Background color changed to blue. The menus look like this:");
                            System.out.println();
                            print_header();
                            main_menu();
                            break;
                        case 6:
                            if(choice == 1 ) colorBackground1 = "\033[45m";
                            else if(choice == 2) colorBackground2 = "\033[45m";
                            else colorBackground3 =  "\033[45m";
                            System.out.println("Background color changed to purple. The menus look like this:");
                            System.out.println();
                            print_header();
                            main_menu();
                            break;
                        case 7:
                            if(choice == 1 ) colorBackground1 = "\033[46m";
                            else if(choice == 2) colorBackground2 = "\033[46m";
                            else colorBackground3 =  "\033[46m";
                            System.out.println("Background color changed to cyan. The menus look like this:");
                            System.out.println();
                            print_header();
                            main_menu();
                            break;
                        case 8:
                            if(choice == 1 ) colorBackground1 = "\033[47m";
                            else if(choice == 2) colorBackground2 = "\033[47m";
                            else colorBackground3 =  "\033[47m";
                            System.out.println("Background color changed to gray. The menus look like this:");
                            System.out.println();
                            print_header();
                            main_menu();
                            break;
                    }
                    System.out.println();
                    System.out.println("If you like to change the background, or change something else, please press 1. Press any other integer to exit");
                    int ret = 1;
                    ret = input.nextShort();
                    if(ret == 1) customize();
                }
            }
            else{
                System.out.println("Please enter a value between 1 and 3");
                customize();
            }
    }
}
