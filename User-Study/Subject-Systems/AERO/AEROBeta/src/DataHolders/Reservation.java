package DataHolders;

import java.util.ArrayList;
import AEROBeta.src.ProjectDB;
//&begin Generate_Ticket
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
//&end Generate_Ticket
public class Reservation extends Person {
    public int flight_number;

    public Reservation(Person person, int flight_number) {
        super(person.name, person.address);
        this.flight_number = flight_number;
    }
    public static int getSCFlightPassengersCount(int flight_num) {
        int counter = 0;
        for (Reservation pa : ProjectDB.reservations_list) {
            if (pa.flight_number == flight_num)
                counter++;
        }
        return counter;
    }
//&begin View_Flight_Reservations
    public static void show_all() {
        int counter = 0;
        for (int i = 0; i < 48; i++)
            System.out.print("-");
        System.out.println();
        System.out.printf("%5s | %-5s | %-30s |\n", "Index", "FN", "Full Name");
        for (int i = 0; i < 48; i++)
            if (i == 6 || i == 14 || i == 47)
                System.out.print("|");
            else
                System.out.print("-");
        System.out.println();

        if (ProjectDB.reservations_list.isEmpty()) {
            System.out.println("\t==> No Reservations added yet <==");
        }

        for (Reservation p : ProjectDB.reservations_list) {
            System.out.printf("%5d | %5d | %-30s |\n", ++counter, p.flight_number, p.name);
        }
        for (int i = 0; i < 48; i++)
            System.out.print("-");
        System.out.println();
    }
//&end View_Flight_Reservations
//&begin View_Passengers_In_Flight
    public static void show_only_flight_no(int flight_num) {
        ArrayList<Reservation> output = new ArrayList<>();
        for (Reservation pa : ProjectDB.reservations_list) {
            if (pa.flight_number == flight_num)
                output.add(pa);
        }

        int counter = 0;
        for (int i = 0; i < 40; i++)
            System.out.print("-");
        System.out.println();

        System.out.printf("%5s | %-30s |\n", "Index", "Full Name");
        for (int i = 0; i < 40; i++)
            if (i == 6 || i == 39)
                System.out.print("|");
            else
                System.out.print("-");
        System.out.println();

        if (output.isEmpty()) {
            System.out.println("\t=> No Reservations added yet <=");
        }

        for (Reservation p : output) {
            System.out.printf("%5d | %-30s |\n", ++counter, p.name);
        }
        for (int i = 0; i < 40; i++)
            System.out.print("-");
        System.out.println();
    }
//&end View_Passengers_In_Flight
//&begin Book_Flight
    public void saveTicket() throws FileNotFoundException, UnsupportedEncodingException {
        String toWrite = "";

        toWrite += "Name: " + super.name + "\n";

        toWrite += "Address: " + super.address + "\n\n";

        toWrite += "*** Flight details *** \n\n";

        for (ScheduledFlight r : ProjectDB.scheduled_flight_list) {
            if (r.flight_number == this.flight_number) {
                toWrite += "Flight number: " + this.flight_number + "\n";
                toWrite += "From: " + r.from + "\n";
                toWrite += "To: " + r.to + "\n";
                toWrite += "Date: " + r.date + "\n";
                toWrite += "Departure: " + r.departure_time + "\n";
                toWrite += "Arrival: " + r.arrival_time + "\n";
                break;
            }
        }
        toWrite += "\n\nThank you for choosing ARS. Hope to see you again!";

        PrintWriter writer = new PrintWriter(new File("Tickets\\" + this.name + "-" + this.flight_number + ".txt"), "UTF-8");
        writer.println(toWrite);
        System.out.println("Ticket generated successfully...");
        writer.close();
    }
//&end Book_Flight
}