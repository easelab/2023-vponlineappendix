package AEROGamma.src;

import DataHolders.FlightDescription;
import DataHolders.Person;
import DataHolders.Reservation;
import DataHolders.ScheduledFlight;

//&begin Import
import java.io.*;
//&end Import

import java.util.ArrayList;

public class ProjectDB {

    public static ArrayList<Person> person_list = new ArrayList<>();
    public static ArrayList<Reservation> reservations_list = new ArrayList<>();
    public static ArrayList<FlightDescription> flight_desc_list = new ArrayList<>();
    public static ArrayList<ScheduledFlight> scheduled_flight_list = new ArrayList<>();
//&begin Feedback
    public static ArrayList<Integer> ratings = new ArrayList<Integer>();
    public static ArrayList<String> reviews = new ArrayList<String>();
//&end Feedback
//&begin Member_Management
    public static void add(Person person) {
        for (Person p : person_list) {
            if (p.name.equals(person.name)) {
                System.out.println("Can't save this data!");
                System.out.println(person.name + " : Already saved!");
                return;
            }
        }
        person_list.add(person);
    }
//&end Member_Management
//&begin Flight_Management
    public static void add(Reservation passenger) {
        for (Reservation p : reservations_list) {
            if (p.flight_number == passenger.flight_number && p.name.equals(passenger.name)) {
                System.out.println("Can't save this data!");
                System.out.println(passenger.name + " : Already reserved this flight!");
                return;
            }
        }
        reservations_list.add(passenger);
    }
    public static void add(FlightDescription flight_desc) {
        for (FlightDescription flight : flight_desc_list) {
            if (flight.arrival_time.equals(flight_desc.arrival_time) &&
                    flight.departure_time.equals(flight_desc.departure_time) &&
                    flight.from.equals(flight_desc.from) &&
                    flight.to.equals(flight_desc.to) &&
                    flight.capacity == flight_desc.capacity) {
                System.out.println("Can't save this data!");
                System.out.println("This Flight description Already exists!");
                return;
            }
        }
        flight_desc_list.add(flight_desc);
    }
    public static void add(ScheduledFlight sc_flight) {
        for (ScheduledFlight flight : scheduled_flight_list) {
            if (flight.arrival_time.equals(sc_flight.arrival_time) &&
                    flight.departure_time.equals(sc_flight.departure_time) &&
                    flight.from.equals(sc_flight.from) &&
                    flight.to.equals(sc_flight.to) &&
                    flight.capacity == sc_flight.capacity &&
                    flight.date.equals(sc_flight.date)) {
                System.out.println("Can't save this data!");
                System.out.println("This Flight Already scheduled!");
                return;
            }
        }
        scheduled_flight_list.add(sc_flight);
    }
//&end Flight_Management
//&begin Import
    public static void loadFromFile() {
        File dartData = new File("allflights.txt");
        if(!dartData.exists()) System.out.println("The file allflights.txt does not exist. Could not load flights...");
        else {
            FileReader fr = null;
            try {
                fr = new FileReader(dartData);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            BufferedReader br = new BufferedReader(fr);

            String line = "";
            while (true) {
                try {
                    if (!((line = br.readLine()) != null)) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String[] flightInfo = line.split(",");
                FlightDescription newFlight = new FlightDescription(flightInfo[0].trim(), flightInfo[1].trim(), flightInfo[2].trim(), flightInfo[3].trim(), Integer.parseInt(flightInfo[4].trim()));
                add(newFlight);
            }
        }
    }
//&end Import
//&begin Feedback
    public static void add(Integer rating) {
        if(rating >= 0 && rating <= 5) {
            ratings.add(rating);
            System.out.println("Thanks for providing your feedback!");
        }
    }
    public static void add(String review) {

        reviews.add(review);
        System.out.println("Thanks for your comment!");
    }
    public static void displayAverageRating(){
        double average = 0.0;
        for(int i = 0; i<ratings.size();i++){
            average += ratings.get(i);
        }
        average = average/ratings.size();
        System.out.println("\n***Average ratings for AEROGamma are: " + average);
    }
    public static void viewAllComments(){
        System.out.println("\n*** Displaying comments...\n");
        for(int i = 0; i<reviews.size();i++){
            System.out.println(reviews.get(i));
        }
        System.out.println();
    }
//&end Feedback
}
