import DataHolders.FlightDescription;
import DataHolders.Person;
import DataHolders.Reservation;
import DataHolders.ScheduledFlight;
import AEROAlpha.src.ProjectDB;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    static ConsoleColors cc = new ConsoleColors();

    public static void main(String[] args) {

        Person person1 = new Person("Michal Scott", "Scranton");
        ProjectDB.add(person1);

        Person person2 = new Person("Dwight Shrute", "Scranton");
        ProjectDB.add(person2);

        Reservation passenger1 = new Reservation(person1, 1);
        ProjectDB.add(passenger1);

        FlightDescription flightDescription1 = new FlightDescription("Frankfurt", "Berlin", "01:00", "02:45", 10);
        ProjectDB.add(flightDescription1);

        ScheduledFlight scheduledFlight1 = new ScheduledFlight(flightDescription1, "2022/06/25");
        ProjectDB.add(scheduledFlight1);

        print_header();
        main_menu();
    }
    private static void exitMessage(){
        System.out.println("Thank you for using airline reservation system");
    }
    private static void print_header() {
        System.out.println(cc.colorBackground1 + "<><><><><><><><><><><><><><><><><><><><>" + cc.RESET);
        System.out.println(cc.colorBackground2 + cc.font1 + "       Airline Reservation System       " + cc.RESET);
        System.out.println(cc.colorBackground1 + "<><><><><><><><><><><><><><><><><><><><>" + cc.RESET);
        System.out.print("\n");
    }
    private static void main_menu() {
        System.out.println(cc.colorBackground3 + cc.font1 + "---------->  Main Menu  <-----------" + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "1- Members Management Menu          " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "2- Flight Management Menu           " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font2 + "3- Exit System                      " + cc.RESET);
        System.out.println(cc.colorBackground3 + cc.font1 + "------------------------------------" + cc.RESET);
        short choice=3;
        Scanner input = new Scanner(System.in);
        do {
            System.out.print("Enter an option between 1 and 3: ");
            try{
                choice = input.nextShort();
                input.nextLine();
            }catch (InputMismatchException e){
                System.out.println();
            }
            switch (choice) {
                case 1:
                    System.out.println("You selected 1. Taking you to the members menu...");
                    System.out.println();
                    membersAndReservationsMenu();
                    break;
                case 2:
                    System.out.println("You selected 2. Taking you to the flight menu...");
                    System.out.println();
                    flights_menu();
                    break;
                case 3:
                    System.out.println("You selected 3. Exiting now...");
                    exitMessage();
                    break;
                default:
                    System.out.println("ERROR: Choice not valid, please enter again!");
            }
        } while (choice < 1 || choice > 3);
    }
    private static void membersAndReservationsMenu() {
        System.out.println(cc.colorBackground3 + cc.font1 +  "---->  Members Management Menu  <----" + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "1- Add a Member                      " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "2- View All Members                  " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "3- Remove a Member                   " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "4- New Reservation                   " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "5- View All Reservations             " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "6- Cancel Reservation                " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font2 + "7- Main Menu                         " + cc.RESET);
        System.out.println(cc.colorBackground3 + cc.font1 + "-------------------------------------" + cc.RESET);
        short choice=7;
        int index;
        Scanner input = new Scanner(System.in);
        do {
            System.out.print("Enter an option between 1 and 7: ");
            try{
                choice = input.nextShort();
                input.nextLine();
            }catch (InputMismatchException e){
                System.out.println("Invalid Choice");
            }

            switch (choice) {
                case 1:
                    System.out.println("---->  NEW Member  <----");
                    input = new Scanner(System.in);
                    System.out.print("Enter Full Name of the New Member: ");
                    String name = input.nextLine();
                    System.out.print("Enter his/her Address: ");
                    String address = input.nextLine();
                {
                    try {
                        ProjectDB.add(new Person(name, address));
                    } catch (Exception ex) {
                        System.out.println("ERROR: File ProjectDB not Found!");
                    }
                }
                System.out.println("New member added successfully : " + name + "\n");
                membersAndReservationsMenu();
                break;
                case 2:
                    System.out.println("=> MEMBERS TABLE  <----");
                    Person.show_all();
                    membersAndReservationsMenu();
                    break;
                case 3:
                    System.out.println("---->  MEMBERS TABLE  <----");
                    Person.show_all();
                    if (ProjectDB.person_list.size() == 0) {
                        membersAndReservationsMenu();
                    }
                    else {
                        do {
                            System.out.print("Member Index to remove : ");
                            index = input.nextInt();
                        } while (index < 1 || index > ProjectDB.person_list.size());
                        ProjectDB.person_list.remove(ProjectDB.person_list.get(index - 1));
                        System.out.println("Member removed Successfully!\n");
                        membersAndReservationsMenu();
                    }
                    break;
                case 4:
                    System.out.println("---->  NEW RESERVATION   <----");
                    Person.show_all();
                    if (ProjectDB.person_list.size() == 0) {
                        System.out.println("No members added yet. Please enter a member and continue. Press 1 to continue!");
                        if(input.nextInt() == 1) membersAndReservationsMenu();
                        else {
                            exitMessage();
                            break;
                        }
                    }
                    else {
                        do {
                            System.out.print("Enter the member index for reservation: ");
                            index = input.nextInt();
                        } while (index < 1 || index > ProjectDB.person_list.size());
                        Person p = ProjectDB.person_list.get(index - 1);
                        ScheduledFlight scf;

                        ScheduledFlight.show_all();
                        if (ProjectDB.scheduled_flight_list.size() == 0) {
                            System.out.println("No flights scheduled yet. Please schedule a flight and continue. Press 1 to continue");
                            if(input.nextInt() == 1) membersAndReservationsMenu();
                            else {
                                exitMessage();
                                break;
                            }
                        }
                        else {
                            do {
                                System.out.print("Enter the flight index for reservation: ");
                                index = input.nextInt();
                            } while (index < 1 || index > ProjectDB.scheduled_flight_list.size());
                            scf = ProjectDB.scheduled_flight_list.get(index - 1);
                            if (scf.capacity == Reservation.getSCFlightPassengersCount(scf.flight_number) || ProjectDB.reservations_list.size() == 0) {
                                System.out.println("This flight is at maximum capacity.");
                            }
                            else {
                                int prevLen = ProjectDB.reservations_list.size();
                                {
                                    try {
                                        Reservation r = new Reservation(p,scf.flight_number);
                                        ProjectDB.add(r);
                                    } catch (Exception ex) {
                                        System.out.println("ERROR : FILE ProjectDB NOT FOUND !");
                                    }
                                }
                                int afterLen = ProjectDB.reservations_list.size();
                                if (prevLen != afterLen) {
                                    System.out.println("Reservation completed : " + p.name + " (" + scf.from + " -> " + scf.to + ")\n");
                                }
                            }
                            membersAndReservationsMenu();
                        }
                    }
                    break;
                case 5:
                    System.out.println("---->  RESERVATIONS TABLE  <----");
                    Reservation.show_all();
                    membersAndReservationsMenu();
                    break;
                case 6:
                    System.out.println("---->  RESERVATIONS TABLE  <----");
                    Reservation.show_all();
                    if (ProjectDB.reservations_list.size() == 0) {
                        membersAndReservationsMenu();
                    }
                    else {
                        do {
                            System.out.print("Reservation Index to Cancel trip for : ");
                            index = input.nextInt();
                        } while (index < 1 || index > ProjectDB.reservations_list.size());
                        ProjectDB.reservations_list.remove(ProjectDB.reservations_list.get(index - 1));
                        System.out.println("Reservation Canceled Successfully!\n");
                        membersAndReservationsMenu();
                    }
                    break;
                case 7:
                    System.out.println("You chose 7. Redirecting you to the main menu...");
                    main_menu();
                    break;
                default:
                    System.out.println("Invalid choice, please enter a number between 1 and 7");
                    break;
            }
        } while (choice < 1 || choice > 7);

    }
    private static void flights_menu() {
        System.out.println(cc.colorBackground3 + cc.font1 + "-------->  Flight Management Menu  <--------" + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "1- Add New Flight Description               " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "2- View All Flight Description              " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "3- Remove Flight Description                " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "4- Schedule New Flight                      " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "5- View All Scheduled Flights               " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "6- Cancel Scheduled Flight                  " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font1 + "7- View Passengers in a Scheduled Flight    " + cc.RESET);
        System.out.println(cc.colorBackground1 + cc.font2 + "8- Main Menu                                " + cc.RESET);
        System.out.println(cc.colorBackground3 + cc.font1 + "--------------------------------------------" + cc.RESET);
        short choice=8;
        int index;
        Scanner input = new Scanner(System.in);
        do {
            System.out.print("Enter an option between 1 and 8: ");
            try{
                choice = input.nextShort();
                input.nextLine();
            }catch (InputMismatchException e){
                System.out.println("Invalid Choice");
            }

            switch (choice) {
                case 1:
                    System.out.println("---->  NEW FLIGHT DESCRIPTION  <----");
                    input = new Scanner(System.in);
                    System.out.print("From : ");
                    String from = input.nextLine();
                    System.out.print("To   : ");
                    String to = input.nextLine();
                    String depTime, arrTime;

                    System.out.print("Departure time (HH:MM): ");
                    depTime = input.nextLine();

                    System.out.print("Arrival   time (HH:MM): ");
                    arrTime = input.nextLine();

                    System.out.print("Capacity : ");
                    input = new Scanner(System.in);
                    int cap = input.nextInt();
                    int prevSize = ProjectDB.flight_desc_list.size();
                {
                    try {
                        ProjectDB.add(new FlightDescription(from, to, depTime, arrTime, cap));
                    } catch (Exception ex) {
                        System.out.println("ERROR: File ProjectDB not Found!");
                    }
                }

                int afterSize = ProjectDB.flight_desc_list.size();
                if (prevSize != afterSize) {
                    System.out.println("Flight Description added successfully : " + from + " -> " + to + "\n");
                    System.out.println("Please note that the flight is not scheduled yet. It will only be displayed in flight descriptions, not scheduled flights.");
                }
                flights_menu();
                break;
                case 2:
                    System.out.println("---->  FLIGHT DESCRIPTION TABLE  <----");
                    FlightDescription.show_all();
                    flights_menu();
                    break;
                case 3:
                    System.out.println("---->  FLIGHT DESCRIPTION TABLE  <----");
                    FlightDescription.show_all();
                    if (ProjectDB.flight_desc_list.size() == 0) {
                        flights_menu();
                    }
                    else {
                        do {
                            System.out.print("Flight description index to remove : ");
                            index = input.nextInt();
                        } while (index < 1 || index > ProjectDB.flight_desc_list.size());
                        ProjectDB.flight_desc_list.remove(ProjectDB.flight_desc_list.get(index - 1));

                        System.out.println("Flight description removed Successfully!\n");
                        flights_menu();
                    }
                    break;
                case 4:
                    System.out.println("---->  FLIGHT DESCRIPTION TABLE  <----");
                    FlightDescription.show_all();
                    if (ProjectDB.flight_desc_list.size() == 0) {
                        flights_menu();
                    }
                    else {
                        do {
                            System.out.print("Flight description index to schedule : ");
                            index = input.nextInt();
                        } while (index < 1 || index > ProjectDB.flight_desc_list.size());
                        FlightDescription fd = ProjectDB.flight_desc_list.get(index - 1);
                        input = new Scanner(System.in);
                        String date;

                        System.out.print("Date (YYYY/MM/DD) : ");
                        date = input.nextLine();

                        int prevLen = ProjectDB.scheduled_flight_list.size();
                        {
                            try {
                                ProjectDB.add(new ScheduledFlight(fd, date));
                            } catch (Exception ex) {
                                System.out.println("ERROR : FILE ProjectDB NOT FOUND !");
                            }
                        }
                        int afterLen = ProjectDB.scheduled_flight_list.size();
                        if (prevLen != afterLen) {
                            System.out.println("Scheduled " + date + " for flight : " + fd.from + " -> " + fd.to + "\n");
                        }
                        flights_menu();
                    }
                    break;
                case 5:
                    System.out.println("---->  SCHEDULED FLIGHTS TABLE  <----");
                    ScheduledFlight.show_all();
                    flights_menu();
                    break;
                case 6:
                    System.out.println("---->  SCHEDULED FLIGHT TABLE  <----");
                    ScheduledFlight.show_all();
                    if (ProjectDB.scheduled_flight_list.size() == 0) {
                        flights_menu();
                    }
                    else {
                        do {
                            System.out.print("Scheduled Flight index to canceled : ");
                            index = input.nextInt();
                        } while (index < 1 || index > ProjectDB.scheduled_flight_list.size());
                        ProjectDB.scheduled_flight_list.remove(ProjectDB.scheduled_flight_list.get(index - 1));
                        System.out.println("Scheduled Flight & Reservations canceled Successfully!\n");
                        flights_menu();
                    }
                    break;
                case 7:
                    System.out.println("---->  SCHEDULED FLIGHT TABLE  <----");
                    ScheduledFlight.show_all();
                    do {
                        System.out.print("Flight Index : ");
                        index = input.nextInt();
                    } while (index < 1 || index > ProjectDB.scheduled_flight_list.size());
                    int flight_num = ProjectDB.scheduled_flight_list.get(index - 1).flight_number;
                    Reservation.show_only_flight_no(flight_num);
                    flights_menu();
                    break;
                case 8:
                    System.out.println("You chose 8. Redirecting you to the main menu...");
                    main_menu();
                    break;
                default:
                    System.out.println("ERROR: Choice not valid");
            }
        } while (choice < 1 || choice > 8);
    }
}
