import java.util.Scanner;

public class ConsoleColors {
    // Reset
    public String RESET = "\033[0m"; // Text Reset

    // Bold
    public String font1 = "\033[1;30m"; // BLACK
    public String font2 = "\033[1;31m"; // RED

    // Background
    public String colorBackground1 = "\033[48;5;243m"; // GRAY
    public String colorBackground2 = "\033[0;101m";// RED
    public String colorBackground3 = "\033[43m"; // YELLOW
}
