public class ConsoleColors {
    // Reset
    public final String RESET = "\033[0m"; // Text Reset

    // Bold
    public final String font1 = "\033[1;30m"; // BLACK
    public final String font2 = "\033[1;31m"; // RED

    // Background
    public final String colorBackground1 = "\033[48;5;243m"; // GRAY
    public final String colorBackground2 = "\033[0;101m";// RED
    public final String colorBackground3 = "\033[43m"; // YELLOW
}
