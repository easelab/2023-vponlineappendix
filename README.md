**Virtual Platform: Effective and Seamless Variability Management for Software Systems - Online appendix**

This site is an online appendix for the paper titled "Virtual Platform: A Framework for Effective and Seamless Variability Management with the Virtual Platform". In the paper, we present a framework: the Virtual Platform, which is a platform for variant management and incremental migration of software variants realized via clone & own into a product-line architecture. The framework works on a set of conceptual structures that are semi-structured representations of code (and other assets) in memory, and a set of operators that exploit those conceptual structures to conduct a risk-free, minimally invasive migration of a set of products into an integrated platform. 

The conceptual structures include assets, features, asset tree, feature model, asset trace database, and feature trace database. Assets are mapped to features via presence conditions. Assets and features can be cloned from one Asset Tree (AT) or Feature Model (FM) into another. An asset clone leads to the addition of an asset trace in the asset trace database, and a feature clone leads to the addition of a feature trace in the feature trace database. Both types of traces hold the version of the source when the asset or feature was cloned. Later, these traces become relevant in automated change propagation, where the version of the source (asset of feature) can be used to determine the last time the source and target were synchronized.

Virtual platform fosters reuse (CloneAsset, CloneFeature) and maintainence (PropagateToAsset, PropagateToFeature) while still allowing developers to deploy clone&own whenever needed. It is incremental, which allows the organization to still operate smoothly while adding feature-related information in the background. Also, having feature-related information in the project provides an overview and allows to integrate configuration mechanism in the project, thereby making it configurable. Feature models allow capturing the various dependencies and cross-tree constraints, providing the ability to check for and avoid invalid configurations.

We build a prototype of the framework consisting of a module for data holding, and another one for all the operators. The programming language we use is Scala, the reason being its simple and concise notation, and the wide range of built-in support for small operations it provides. It is also easily interoperable with different frameworks. We integrated Kiama in virtual platform, which is a tree writing framework providing efficient tree traversal. 

For our **simulation study**, we created a separate module, which consisted of code parsers (for JavaScript and Json), feature model handlers, git commit history executor, and logging module etc. Specifically, the module was designed to replay the commit history of an already developed project (i.e., Clafer Web Tools with 4 sub-systems), and see how the costs of adding features compared to the cost savings due to the added features. Specifically, we retrofitted the tasks performed by the developer in one branch of the 4 projects to the operators we define. This branch was a simulation of the actual development, where one developer was consulting with the other developers who worked on the main development, and reading the documentation to understand the source code of the projects and annotate them with feature-related information. We replayed 357 commits, and after every commit, the state of the repository was consistent with the state of the asset tree managed by the virtual platform.

For our **user study**, we implemented a Java parser that parsed Java code with embedded feature annotations. Next, we extended the virtual platform by implementing trace database persistence, serialization, and code-level cloning and change propagation. Lastly, we implemented a picocli-based command-line interface to conduct a user study with the virtual platform. Specifically, we conducted he user study with 12 participants, requiring them to perform routine developer tasks using two modes of work: manually and using the virtual platform. We compared the two modes of work based on their correctness and time efficiency. Lastly, we asked participants to provide their subjective feedback both in the form of survey question responses as well as interview question responses.


**Online appendix**

Our online appendix (VirtualPlatformOnlineAppendix.pdf) entails the details of our framework, including algorithms and technical explanations of the operators. We also summarize the related work and comparatively analyze our framework with the existing frameworks on a few properties. 

**Replication Package**

1. Virtual platform's source code can be found at hhttps://bitbucket.org/easelab/workspace/projects/VP.

2. The considered dataset from Ji et al. consists of four Git repositories hosted on GitHub: ClaferIDE, ClaferMooVisualizer, ClaferConfigurator, ClaferToolsUICommonPlatform 
In each repository, the feature information was added by the developer of the dataset within commits on a dedicated branch called "reenactPilot2".

3. For the simulation study, we provide detailed data from our evaluation, including a summary log with coarse-grained information on all commits (outputlog.txt), and fine-grained logs and corresponding AT snapshots for each commit (in Simulation-Study/ExperimentData).

4. For the user study, we provide the supplementary materials (User-Study/Supplementary-Material), specifically the intructions (for group 1 and 2), the installation guide (VPInstallationGuide.txt), an overview of the virtual platform (VirtualPlatformIntroduction.ptx), descriptions of the subject systems (SubjectSystemsIntroduction.pdf), and the format for invoking operators (OperatorsUsage.pdf). We also share the questionnaires.

5. Additionally, for the user study, we provide the experimental data (User-Study/Data-Raw-and-Processed), the source code for the subject systems (User-Study/Subject-Systems), as well as the scripts for analyzing the data and creating visualizations (User-Study/Scripts).



